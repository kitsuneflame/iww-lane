---
title: "Saturday Market: 13 October"
date: "2018-10-09"
description: ""
# thumbnail: "img/placeholder.jpg" # Optional, thumbnail
lead: "Come join us downtown!"
disable_comments: true # Optional, disable Disqus comments if true
authorbox: false # Optional, enable authorbox for specific post
toc: false # Optional, enable Table of Contents for specific post
mathjax: false # Optional, enable MathJax for specific post
categories:
  - "Events"
tags:
  - "event"
  - "Another test"
---

We will be hosting a table at the Eugene Saturday Market on 13 October 2018 from 9:30 AM to 5:00 PM.  We will be handing out information on workers rights but also how you can play an active part in organizing not only your workplace but your community.  We'll also be there to answer your questions, discuss current events and concerns in our community, and just chat and much more!
