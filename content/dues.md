# Dues & Donations

## Pay Here via PayPal
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="VA78GVNYTHQUC" />
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Buy Now" />
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

## Dues
The current dues for IWW Memebership are as follows:

- $11:  income below $2000 per month.
- $22:  income between $2000 - $3500 per month.
- $33:  income above $3500 per month.

If you are joining the IWW, there is an addition initiation fee equal to your first month due.


## Donations
If you wish to help give to our cause of helping organize workers of Lane County and cannot join our organization consider making a donation.  You money will help fund organization costs to assist workers in unionizing.  These costs include things like funding for training classes, paying for promotional materials during campaigns, participation in local events, and giving material support to other local organizations such as the [Eugene-Springfield Solidarity Network](https://www.facebook.com/groups/166453220083281/).

All spending is voted on by our memebers at our monthly meetings.

The Industrial Workers of the World and its member organizations are 501(c)(5) Labor and Agricultural Organization and **does not qualify as tax deductable.**
